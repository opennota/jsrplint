// vim: sw=4 ts=4 sts=4 et:
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

const test = require('tape');
const {
    removeRedundantParens
} = require('../src/jsrplint.js');

const testCases = [
    ['(a)', 'a'],
    ['((a))', 'a'],
    ['(((a)))', 'a'],
    ['if(a);', 'if(a);'],
    ['function f(a){}', 'function f(a){}'],
    ['let f = (a) => b;', 'let f = a => b;'],
    ['let f = (a,b) => c;', 'let f = (a,b) => c;'],
    ['(a+b)', 'a+b'],
    ['(a+b)*c', '(a+b)*c'],
    ['a*(b+c)', 'a*(b+c)'],
    ['(a*b)-c', 'a*b-c'],
    ['a+(b*c)', 'a+b*c'],
    ['(a || b)', 'a || b'],
    ['(a) || (b)', 'a || b'],
    ['((a) && (b))', 'a && b'],
    ['(a && b) || c', 'a && b || c'],
    ['a || (b && c)', 'a || b && c'],
    ['(a || b) && c', '(a || b) && c'],
    ['a && (b || c)', 'a && (b || c)'],
    ['(a ^ b) == (c ^ d)', '(a ^ b) == (c ^ d)'],
    ['a ^ (b == c) ^ d', 'a ^ b == c ^ d'],
    ['switch(a) { case(b): }', 'switch(a) { case b: }'],
    ['delete(a.x);', 'delete a.x;'],
    ['if(a)(b);else(c);', 'if(a)b;else c;'],
    ['class A extends(B){}', 'class A extends B {}'],
    ['for (let a in ([...b]));', 'for (let a in [...b]);'],
    ['a instanceof(A);', 'a instanceof A;'],
    ['a instanceof (A);', 'a instanceof A;'],
    ['function f() { return(a); }', 'function f() { return a; }'],
    ['function f() { throw(a); }', 'function f() { throw a; }'],
    ['typeof(a);', 'typeof a;'],
    ['(void(a));', 'void a;'],
    ['function*f() { yield(a); }', 'function*f() { yield a; }'],
    ['try{}catch(e){}', 'try{}catch(e){}'],
    ['for(;;);', 'for(;;);'],
    ['while(a);', 'while(a);'],
    ['while((a));', 'while(a);'],
    ['"()";', '"()";'],
    ['")";', '")";'],
    ["')';", "')';"],
    ['`)`;', '`)`;'],
    ['(/\\)/);', '/\\)/;'],
    ['({")":a})', '({")":a})'],
    ['let a = {")":(b)}', 'let a = {")":b}'],
    ['"(";(`)`+a);', '"(";`)`+a;'],
    ['(a=>b)', 'a=>b'],
    ['(a=>({b}))', 'a=>({b})'],
    ['(a=>(({b})))', 'a=>({b})'],
    ['(a => ((({b}))))', 'a => ({b})'],
    ['a++ +(+b)', 'a++ + +b'],
    ['a---(-b)', 'a--- -b'],
    ['a++-(+b)', 'a++-+b'],
    ['a--+(-b)', 'a--+-b'],
    ['a+(b+(c+d))', 'a+(b+(c+d))'],
    ['((a+b)+c)+d', 'a+b+c+d'],
];

test('Removing parentheses', t => {
    for (let tc of testCases) {
        t.equal(removeRedundantParens(tc[0]), tc[1], `removeRedundantParens('${tc[0]}') = ${tc[1]}`);
    }
    t.end();
});
