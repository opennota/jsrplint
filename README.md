jsrplint [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
========

A linter which finds redundant parentheses in JavaScript code (and can remove them).

# Install

    git clone https://gitlab.com/opennota/jsrplint
    cd jsrplint
    npm install

*Note*: the `npm install` [documentation](https://docs.npmjs.com/cli/install) claims that one can install a GitHub repository simply by

    npm install github:username/repo

or even shorter

    npm install username/repo

Neither of the commands (`username/repo` replaced by `opennota/jsrplint`) has worked for me.

# Use

Find redundant parentheses:

    jsrplint [FILENAME]

Remove them:

    jsrplint --rm [FILENAME]

# Examples

    $ echo "var a = (b && c) || (d && e)" | jsrplint
    -: line 1, col 9, Parentheses could be removed.
    -: line 1, col 21, Parentheses could be removed.

    $ echo "var a = (b && c) || (d && e)" | jsrplint --rm
    var a = b && c || d && e

