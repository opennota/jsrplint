// vim: sw=4 ts=4 sts=4 et:
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

const {
    parseScript,
    parseScriptWithLocation
} = require('shift-parser');

function alphaNum(ch) {
    return '0' <= ch && ch <= '9' ||
        'a' <= ch && ch <= 'z' ||
        'A' <= ch && ch <= 'Z' ||
        ch == '_' || ch == '$';
}

function endsWithKeyword(str, end) {
    let i = Math.min(end - 10, 0);
    let s = str.substring(i, end + 1);
    return /(^|[^a-zA-Z0-9_$])(case|delete|else|extends|in|instanceof|return|throw|typeof|void|yield)$/.test(s);
}

function findParens(str, skipRanges) {
    let result = [];
    let stack = [];
    let line = 1,
        col = 0;
    let prevNonspace;
    let prevIndex;
    let skip;
    for (let i = 0; i < str.length; i++) {
        let ch = str[i];

        if (ch == '\n') {
            line++;
            col = 0;
            if (skip)
                skip--;
            continue;
        }
        col++;

        if (skip) {
            skip--;
            continue;
        }

        let end = skipRanges[i];
        if (end) {
            skip = end - i - 1;
            continue;
        }

        if (ch == '(') {
            stack.push({
                start: i,
                line: line,
                col: col,
                skip: alphaNum(prevNonspace) && !endsWithKeyword(str, prevIndex) ||
                    /=>\s*\(\s*$/.test(str.substring(0, i + 1)) &&
                    /^(?:\(\s*)+{/.test(str.substring(i + 1))
            });
        } else if (ch == ')') {
            let e = stack.pop();
            if (!e.skip && i > e.start + 1) {
                e.end = i;
                result.push(e);
            }
        }

        if (ch != ' ' && ch != '\t') {
            prevNonspace = ch;
            prevIndex = i;
        }
    }
    if (stack.length)
        throw "Oops! Unclosed parentheses!";
    return result;
}

let aa = [];

function visit(tree, v) {
    if (v && !v(tree))
        return;

    switch (tree.type) {
        case 'ArrayAssignmentTarget':
            for (let i = 0; i < tree.elements.length; i++)
                if (tree.elements[i])
                    visit(tree.elements[i], v);
            if (tree.rest)
                visit(tree.rest, v);
            break;
        case 'ArrayBinding':
            for (let i = 0; i < tree.elements.length; i++)
                if (tree.elements[i])
                    visit(tree.elements[i], v);
            if (tree.rest)
                visit(tree.rest, v);
            break;
        case 'ArrayExpression':
            for (let i = 0; i < tree.elements.length; i++)
                if (tree.elements[i])
                    visit(tree.elements[i], v);
            break;
        case 'ArrowExpression':
            visit(tree.params, v);
            visit(tree.body, v);
            break;
        case 'AssignmentExpression':
            visit(tree.binding, v);
            visit(tree.expression, v);
            break;
        case 'AssignmentTargetIdentifier':
            break;
        case 'AssignmentTargetPropertyIdentifier':
            visit(tree.binding, v);
            if (tree.init)
                visit(tree.init, v);
            break;
        case 'AssignmentTargetPropertyProperty':
            visit(tree.name, v);
            visit(tree.binding, v);
            break;
        case 'AssignmentTargetWithDefault':
            visit(tree.binding, v);
            visit(tree.init, v);
            break;
        case 'BinaryExpression':
            visit(tree.left, v);
            visit(tree.right, v);
            break;
        case 'BindingIdentifier':
            break;
        case 'BindingPropertyIdentifier':
            if (tree.init)
                visit(tree.init, v);
            break;
        case 'BindingPropertyProperty':
            visit(tree.name, v);
            visit(tree.binding, v);
            break;
        case 'BindingWithDefault':
            visit(tree.binding, v);
            visit(tree.init, v);
            break;
        case 'Block':
            for (let i = 0; i < tree.statements.length; i++)
                visit(tree.statements[i], v);
            break;
        case 'BlockStatement':
            visit(tree.block, v);
            break;
        case 'BreakStatement':
            break;
        case 'CallExpression':
            visit(tree.callee, v);
            for (let i = 0; i < tree.arguments.length; i++)
                visit(tree.arguments[i], v);
            break;
        case 'CatchClause':
            visit(tree.binding, v);
            visit(tree.body, v);
            break;
        case 'ClassDeclaration':
            visit(tree.name, v);
            if (tree.super)
                visit(tree.super, v);
            for (let i = 0; i < tree.elements.length; i++)
                visit(tree.elements[i], v);
            break;
        case 'ClassElement':
            visit(tree.method, v);
            break;
        case 'ClassExpression':
            if (tree.super)
                visit(tree.super, v);
            for (let i = 0; i < tree.elements.length; i++)
                visit(tree.elements[i], v);
            break;
        case 'CompoundAssignmentExpression':
            visit(tree.binding, v);
            visit(tree.expression, v);
            break;
        case 'ComputedMemberAssignmentTarget':
            visit(tree.object, v);
            visit(tree.expression, v);
            break;
        case 'ComputedMemberExpression':
            visit(tree.object, v);
            visit(tree.expression, v);
            break;
        case 'ComputedPropertyName':
            visit(tree.expression, v);
            break;
        case 'ConditionalExpression':
            visit(tree.test, v);
            visit(tree.consequent, v);
            if (tree.alternate)
                visit(tree.alternate, v);
            break;
        case 'ContinueStatement':
            break;
        case 'DataProperty':
            visit(tree.name, v);
            visit(tree.expression, v);
            break;
        case 'DebuggerStatement':
            break;
        case 'Directive':
            break;
        case 'DoWhileStatement':
            visit(tree.body, v);
            visit(tree.test, v);
            break;
        case 'EmptyStatement':
            break;
            //case 'Export':
            //case 'ExportAllFrom':
            //case 'ExportDefault':
            //case 'ExportFrom':
            //case 'ExportFromSpecifier':
            //case 'ExportLocalSpecifier':
            //case 'ExportLocals':
        case 'ExpressionStatement':
            visit(tree.expression, v);
            break;
        case 'ForInStatement':
            visit(tree.left, v);
            visit(tree.right, v);
            visit(tree.body, v);
            break;
        case 'ForOfStatement':
            visit(tree.left, v);
            visit(tree.right, v);
            visit(tree.body, v);
            break;
        case 'ForStatement':
            if (tree.init)
                visit(tree.init, v);
            if (tree.test)
                visit(tree.test, v);
            if (tree.update)
                visit(tree.update, v);
            visit(tree.body, v);
            break;
        case 'FormalParameters':
            for (let i = 0; i < tree.items.length; i++)
                visit(tree.items[i], v);
            if (tree.rest)
                visit(tree.rest, v);
            break;
        case 'FunctionBody':
            for (let i = 0; i < tree.directives.length; i++)
                visit(tree.directives[i], v);
            for (let i = 0; i < tree.statements.length; i++)
                visit(tree.statements[i], v);
            break;
        case 'FunctionDeclaration':
            visit(tree.name, v);
            visit(tree.params, v);
            visit(tree.body, v);
            break;
        case 'FunctionExpression':
            visit(tree.params, v);
            visit(tree.body, v);
            break;
        case 'Getter':
            visit(tree.name, v);
            visit(tree.body, v);
            break;
        case 'IdentifierExpression':
            break;
        case 'IfStatement':
            visit(tree.test, v);
            visit(tree.consequent, v);
            if (tree.alternate)
                visit(tree.alternate, v);
            break;
            //case 'Import':
            //case 'ImportNamespace':
            //case 'ImportSpecifier':
        case 'LabeledStatement':
            visit(tree.body, v);
            break;
        case 'LiteralBooleanExpression':
            break;
        case 'LiteralInfinityExpression':
            break;
        case 'LiteralNullExpression':
            break;
        case 'LiteralNumericExpression':
            break;
        case 'LiteralRegExpExpression':
            break;
        case 'LiteralStringExpression':
            break;
        case 'Method':
            visit(tree.name, v);
            visit(tree.params, v);
            visit(tree.body, v);
            break;
            //case 'Module':
        case 'NewExpression':
            visit(tree.callee, v);
            for (let i = 0; i < tree.arguments.length; i++)
                visit(tree.arguments[i], v);
            break;
        case 'NewTargetExpression':
            break;
        case 'ObjectAssignmentTarget':
            for (let i = 0; i < tree.properties.length; i++)
                visit(tree.properties[i], v);
            break;
        case 'ObjectBinding':
            for (let i = 0; i < tree.properties.length; i++)
                visit(tree.properties[i], v);
            break;
        case 'ObjectExpression':
            for (let i = 0; i < tree.properties.length; i++)
                visit(tree.properties[i], v);
            break;
        case 'ReturnStatement':
            if (tree.expression)
                visit(tree.expression, v);
            break;
        case 'Script':
            for (let i = 0; i < tree.directives.length; i++)
                visit(tree.directives[i], v);
            for (let i = 0; i < tree.statements.length; i++)
                visit(tree.statements[i], v);
            break;
        case 'Setter':
            visit(tree.name, v);
            visit(tree.param, v);
            visit(tree.body, v);
            break;
        case 'ShorthandProperty':
            visit(tree.name, v);
            break;
        case 'SpreadElement':
            visit(tree.expression, v);
            break;
        case 'StaticMemberAssignmentTarget':
            visit(tree.object, v);
            break;
        case 'StaticMemberExpression':
            visit(tree.object, v);
            break;
        case 'StaticPropertyName':
            break;
        case 'Super':
            break;
        case 'SwitchCase':
            visit(tree.test, v);
            for (let i = 0; i < tree.consequent.length; i++)
                visit(tree.consequent[i], v);
            break;
        case 'SwitchDefault':
            for (let i = 0; i < tree.consequent.length; i++)
                visit(tree.consequent[i], v);
            break;
        case 'SwitchStatement':
            visit(tree.discriminant, v);
            for (let i = 0; i < tree.cases.length; i++)
                visit(tree.cases[i], v);
            break;
        case 'SwitchStatementWithDefault':
            visit(tree.discriminant, v);
            for (let i = 0; i < tree.preDefaultCases.length; i++)
                visit(tree.preDefaultCases[i], v);
            visit(tree.defaultCase, v);
            for (let i = 0; i < tree.postDefaultCases.length; i++)
                visit(tree.postDefaultCases[i], v);
            break;
        case 'TemplateElement':
            break;
        case 'TemplateExpression':
            for (let i = 0; i < tree.elements.length; i++)
                visit(tree.elements[i], v);
            break;
        case 'ThisExpression':
            break;
        case 'ThrowStatement':
            visit(tree.expression, v);
            break;
        case 'TryCatchStatement':
            visit(tree.body, v);
            visit(tree.catchClause, v);
            break;
        case 'TryFinallyStatement':
            visit(tree.body, v);
            if (tree.catchClause)
                visit(tree.catchClause, v);
            visit(tree.finalizer, v);
            break;
        case 'UnaryExpression':
            visit(tree.operand, v);
            break;
        case 'UpdateExpression':
            visit(tree.operand, v);
            break;
        case 'VariableDeclaration':
            for (let i = 0; i < tree.declarators.length; i++)
                visit(tree.declarators[i], v);
            break;
        case 'VariableDeclarationStatement':
            visit(tree.declaration, v);
            break;
        case 'VariableDeclarator':
            visit(tree.binding, v);
            if (tree.init)
                visit(tree.init, v);
            break;
        case 'WhileStatement':
            visit(tree.test, v);
            visit(tree.body, v);
            break;
        case 'WithStatement':
            visit(tree.object, v);
            visit(tree.body, v);
            break;
        case 'YieldExpression':
            if (tree.expression)
                visit(tree.expression, v);
            break;
        case 'YieldGeneratorExpression':
            visit(tree.expression, v);
            break;
        default:
            throw tree.type;
    }
}

const skipTypes = ['LiteralStringExpression', 'LiteralRegExpExpression', 'TemplateExpression', 'Directive', 'StaticPropertyName'];

function findRedundantParens(code) {
    const prepend = 'function*g(){', append = '}'; // to allow bare yield and return
    const off = prepend.length;
    let result = [];
    let {
        tree,
        locations,
        comments
    } = parseScriptWithLocation(prepend+code+append);
    let orig = JSON.stringify(tree);

    let skip = {};
    for (let c of comments)
        skip[c.start.offset-off] = c.end.offset-off;
    visit(tree, node => {
        if (skipTypes.includes(node.type)) {
            let l = locations.get(node);
            skip[l.start.offset-off] = l.end.offset-off;
        }
        return true;
    });

    for (let p of findParens(code, skip)) {
        let c =
            code.substring(0, p.start) + ' ' +
            code.substring(p.start + 1, p.end) + ' ' +
            code.substring(p.end + 1);
        try {
            let newTree = parseScript(prepend+c+append);
            if (JSON.stringify(newTree) == orig)
                result.push(p);
        } catch (e) {}
    }

    result.sort((a, b) => a.line - b.line || a.col - b.col);

    return result;
}

exports.findRedundantParens = findRedundantParens;

exports.removeRedundantParens = code => {
    let pos = [];
    for (let {
            start,
            end
        } of findRedundantParens(code)) {
        pos.push(start, end);
    }
    pos.sort((a, b) => a - b);
    let i = 0;
    let result = '';
    for (let k = 0; k < pos.length; k++) {
        let j = pos[k];
        let ss = code.substring(i, j);
        result += ss;
        if (alphaNum(code[j - 1]) && (code[j + 1] == '{' || alphaNum(code[j + 1])) || ['+', '-'].includes(code[j - 1]) && code[j + 1] == code[j - 1]) {
            result += ' ';
        }
        i = j + 1;
    }
    result += code.substring(i);
    return result;
};
